//
//  ViewController.swift
//  CustomProtocolTest
//
//  Created by Reid Belton on 7/7/15.
//  Copyright (c) 2015 NTENT. All rights reserved.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var forwardButton: UIButton!
    
    @IBAction func reloadButtonPressed(sender: UIButton) {
        webView.reload()
        inputField.resignFirstResponder()
    }
    
    @IBAction func backButtonPressed(sender: UIButton) {
        webView.goBack()
    }
    
    @IBAction func forwardButtonPressed(sender: UIButton) {
        webView.goForward()
    }
    
    private func loadURL() {
        if (count(inputField.text) < 1) {
            return
        }
        
        if (!inputField.text.hasPrefix("http://")) {
            inputField.text = "http://\(inputField.text)"
        }
        
        if let URL = NSURL(string: inputField.text) {
            let request = NSURLRequest(URL: URL)
            webView.loadRequest(request)
        }
    }
    
    func updateNavButtons() {
        backButton.enabled = webView.canGoBack
        forwardButton.enabled = webView.canGoForward
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(textField: UITextField) {
        updateNavButtons()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        loadURL()
        inputField.resignFirstResponder()
        
        return true
    }
}

extension ViewController: UIWebViewDelegate {
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        updateNavButtons()
        
        println("Webview did fail load with error: \(error)")
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        updateNavButtons()
        
        println("Webview did finish load")
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        updateNavButtons()
        
        println("shouldStartLoad with request: \(request), type: \(navigationType)")
        
        return true
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        updateNavButtons()
        
        println("didStartLoad")
    }
}

